#!python3
# A text-based 999 clone
from datetime import datetime
import random

class Door(object):
    def __init__(self, number: int):
        self.number = number
        self.open = False
        self.DEAD = False
        self.TIME_LIMIT = 81 # seconds
        self.timer = 0
        self.bracelet_combination = []

    def open_door(self, bracelets: list):
        """
            Takes a list of bracelets and checks if their
            digital root is equal to the door's value
        """
        if self.open:
            return 'Error!'
        if 3 <= len(bracelets) <= 5:
            droot = sum(bracelets)%9

            if not droot: # If the sum is 9*n, (9*n)%9==0
                droot = 9
            
            if droot == self.number:
                self.open = True
                self.bracelet_combination = bracelets
                self.timer = datetime.now()
                return 'Engaged'
        else:
            return 'Error!'

    def check_timer(self):
        """
            Check if the door's time limit has been reached
        """
        if (datetime.now() - self.timer).total_seconds() >= self.TIME_LIMIT:
            if not self.DEAD:
                for player in self.bracelet_combination:
                    player.execute()

    def DEAD_verify(self, bracelets):
        if self.bracelet_combination == bracelets:
            self.DEAD = True
            self.timer = 0
            

class Character(object):
    def __init__(self, name: str):
        self.name = name
        self.isalive = True

class Player(Character):
    def __init__(self, name: str, bracelet: int):
        self.name = name
        self.bracelet = bracelet
        self.isalive = True

    def __add__(self, x): # To make bracelet comparisons easier
        return self.bracelet + x
    def __radd__(self, x):
        return self.bracelet + x
    def __iadd__(self, x):
        return self.bracelet + x

    def execute(self):
        self.isalive = False

    def action(self):
        def dummy_act():
            print("{} does nothing.".format(self.name))
        def kill(self):
            target = False
            while not target:
                target = random.choice(game.player)
                if target == self:
                    target = False
            target.execute()
            print("{} kills {} with a spoon.".format(self.name,
                                                     target.name))
        def chat(self):
            pass
        def joke(self):
            pass
        def freak_out(self):
            pass
            
        acts = [self.kill,
                self.chat,
                self.joke,
                self.freak_out]
        event = random.choice([random.choice(acts)
                               if i%3==0
                               else dummy_act
                               for i in range(self.seed[0])])
        event()

class Zero(Player):
    def __init__(self, identity):
        self.name = 'Zero'
        self.identity = identity
        self.bracelet = 0
        self.isalive = self.identity.isalive
        
    def execute(self):
        self.identity.isalive = False
        self.isalive = False

class NonaryGame(object):
    def __init__(self, *seed: list):
        if not seed:
            self.seed = [random.randint(0,9) for i in range(10)]
        else:
            self.seed = seed
        self.door = [Door(i) for i in range(1,10)]
        self.player = [
            Player('Ace', 1),
            Player('Snake', 2),
            Player('Santa', 3),
            Player('Clover', 4),
            Player('Junpei', 5),
            Player('June', 6),
            Player('Seven', 7),
            Player('Lotus', 8),
            Player('The ninth man', 9),
            ]
        self.player.insert(0, Zero(random.choice(self.player)))
        print("{} is Zero!".format(self.player[0].identity.name))
        print("Zero is alive? {}.".format(self.player[0].isalive))
        print("Executing {}...".format(self.player[0].identity.name))
        self.player[0].execute()
        print("{} is alive? {}.\nZero is alive? {}.".format(self.player[0].identity.name,
                                                           self.player[0].identity.isalive,
                                                           self.player[0].isalive))
    def doors(self):
        pass        

    def run(self):
        pass

def checks():
    door = [Door(i) for i in range(1, 10)]
    player = [
             Player('Ace', 1),
             Player('Snake', 2),
             Player('Santa', 3),
             Player('Clover', 4),
             Player('Junpei', 5),
             Player('June', 6),
             Player('Seven', 7),
             Player('Lotus', 8),
             Player('The ninth man', 9),
             ]
    print(player[5] + player[1] + player[3])
    print(player[5].bracelet)
    game = NonaryGame()

if __name__ == '__main__':
    checks()
